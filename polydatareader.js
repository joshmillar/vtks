document.body.style.setProperty('opacity', 0);
const fullScreenRenderer = vtk.Rendering.Misc.vtkFullScreenRenderWindow.newInstance({background:[1,1,1]});
const vtkColorMaps = vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps;
fullScreenRenderer.addController('<table><tr><td>Contours</td><td><input class="contours" type="checkbox" checked name="contourcheck"/></td></tr><tr><td>Streamlines</td><td><input class="streamlines" type="checkbox" name="streamcheck"/></td></tr></table>');

//set controls
const interactorStyle = vtk.Interaction.Style.vtkInteractorStyleManipulator.newInstance();
const roll = vtk.Interaction.Manipulators.vtkMouseCameraTrackballRotateManipulator.newInstance({ button: 3, });
const pan = vtk.Interaction.Manipulators.vtkMouseCameraTrackballPanManipulator.newInstance({ button: 1, });
const zoom = vtk.Interaction.Manipulators.vtkMouseCameraTrackballZoomManipulator.newInstance({ button: 3, control: true }  );
const zoomscroll = vtk.Interaction.Manipulators.vtkMouseCameraTrackballZoomToMouseManipulator.newInstance( {scrollEnabled: true, dragEnabled: false } );
interactorStyle.removeAllMouseManipulators();
interactorStyle.addMouseManipulator(roll);
interactorStyle.addMouseManipulator(pan);
interactorStyle.addMouseManipulator(zoom);
interactorStyle.addMouseManipulator(zoomscroll);
fullScreenRenderer.getInteractor().setInteractorStyle(interactorStyle);

//<tr><td><button class="viewbutton" name="viewbutton" style="width: 100%">Reset camera</button></td></tr>
let url = 'https://joshmillar.gitlab.io/vtks/static/col_vel_small.vtp';

const vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

console.log(vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps.rgbPresetNames);

const lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
// const preset = vtkColorMaps.getPresetByName('erdc_rainbow_bright');
// const preset = vtkColorMaps.getPresetByName('Rainbow Desaturated');
const preset = vtkColorMaps.getPresetByName('jet');
lookupTable.applyColorMap(preset);
lookupTable.setDiscretize(true);
lookupTable.setNumberOfValues(16);
// lookupTable.setMappingRange([0, 10])
// lookupTable.updateRange();

const renderer = fullScreenRenderer.getRenderer();
// renderer.getActiveCamera().setPosition(-2163, -1288, 2500);
// renderer.getActiveCamera().setFocalPoint(8323, 5071, -6336);
// renderer.getActiveCamera().setViewUp(0.45, 0.29, 0.8447);
// renderer.getActiveCamera().zoom(0.5);
renderer.updateLightsGeometryToFollowCamera();
const renderWindow = fullScreenRenderer.getRenderWindow();


var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: false,
        useLookupTableScalarRange: false,
        lookupTable,
        scalarVisibility: true,
});


const arrayName = 'Vel';

vtkMapper.setScalarModeToUsePointFieldData();
// vtkMapper.setScalarModeToDefault();
vtkMapper.setColorModeToMapScalars();
vtkMapper.setColorByArrayName(arrayName);
vtkMapper.setScalarRange([0, 15])

// console.log(mapper.getBounds());
// console.log(mapper.getColorByArrayName());
// console.log(mapper.getFieldDataTupleId());
// console.log(mapper);
var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();

vtkMapper.setInputConnection(vtpReader.getOutputPort());
vtkActor.setMapper(vtkMapper);

vtpReader.setUrl(url).then(() => {
    renderer.addActor(vtkActor);
    // renderer.getActiveCamera().setParallelScale(980);
    renderer.resetCamera();
    renderWindow.render();
    document.body.style.setProperty('opacity', 1);
    // document.body.classList.remove('.loading')
});

var contourcheckbox = document.querySelector("input[name=contourcheck]")

contourcheckbox.addEventListener('change', function() {
    if(this.checked){
        vtpReader.setUrl(url).then(() => {
        renderer.addActor(vtkActor);
        // renderer.getActiveCamera().setParallelScale(980);
        renderWindow.render();
        // document.body.css.setProperty(‘opacity’, 0)
        // document.querySelector('.loading').classList.remove('loading');
        });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});

// // stls
let stlurl = 'https://joshmillar.gitlab.io/vtks/static/BDGS.stl';
var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
    scalarVisibility: false,
});
var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

stlActor.setMapper(stlMapper);
stlMapper.setInputConnection(stlReader.getOutputPort());
stlActor.getProperty().setColor(1, 1, 1);
// stlActor.getProperty().setEdgeVisibility(false);
// stlActor.getProperty().setEdgeColor(0, 0, 0);

stlReader.setUrl(stlurl).then(() => {
    renderer.addActor(stlActor);
    renderer.resetCamera();
    renderWindow.render();
});

// extract feature edges
let edgeurl = 'https://joshmillar.gitlab.io/vtks/static/edges.vtp';

var edgeMapper = vtk.Rendering.Core.vtkMapper.newInstance({
    scalarVisibility: false,
});
var edgeActor = vtk.Rendering.Core.vtkActor.newInstance();
var edgeReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

edgeActor.setMapper(edgeMapper);
edgeMapper.setInputConnection(edgeReader.getOutputPort());
edgeActor.getProperty().setColor(0, 0, 0);

edgeReader.setUrl(edgeurl).then(() => {
    renderer.addActor(edgeActor);
    renderer.resetCamera();
    renderWindow.render();
})

// extract road edges
// let roadurl = 'https://joshmillar.gitlab.io/vtks/static/col_roads.vtp';

// var roadMapper = vtk.Rendering.Core.vtkMapper.newInstance({
//     scalarVisibility: false,
// });
// var roadActor = vtk.Rendering.Core.vtkActor.newInstance();
// var roadReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

// roadActor.setMapper(roadMapper);
// roadMapper.setInputConnection(roadReader.getOutputPort());
// roadActor.getProperty().setColor(0, 0, 0);

// roadReader.setUrl(roadurl).then(() => {
//     renderer.addActor(roadActor);
//     renderer.resetCamera();
//     renderWindow.render();
// })


// fullScreenRenderer.addController('<table><tr><td>Streamlines</td><td><input class="streamlines" type="checkbox" name="streamcheck"/></td></tr></table>');

var streamcheckbox = document.querySelector("input[name=streamcheck]")

let streamurl = 'https://joshmillar.gitlab.io/vtks/static/col_streamlines_270.vtp';
var streamMapper = vtk.Rendering.Core.vtkMapper.newInstance({
    scalarVisibility: true,
});
streamMapper.setScalarModeToDefault();
streamMapper.setColorModeToMapScalars();
streamMapper.setScalarRange([0, 12])
var streamActor = vtk.Rendering.Core.vtkActor.newInstance();
var streamReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

streamActor.setMapper(streamMapper);
streamMapper.setInputConnection(streamReader.getOutputPort());

streamcheckbox.addEventListener('change', function() {
	if(this.checked){
		streamReader.setUrl(streamurl).then(() => {
            renderer.addActor(streamActor);
            renderWindow.render();
        });
	} else {
		renderer.removeActor(streamActor);
        renderWindow.render();
	}
});

// var resetbutton = document.querySelector(".viewbutton");

// resetbutton.addEventListener('click', function() {
//     renderer.addActor(edgeActor);
//     renderer.resetCamera();
// });

// document.querySelector('.edgeVisibility').addEventListener('change', (e) =>{
//     const edgeVisibility = !!e.target.checked;
//     stlActor.getProperty().setEdgeVisibility(edgeVisibility);
//     console.log(stlActor.getProperty().setEdgeVisibility(edgeVisibility));
//     renderer.resetCamera();
//     renderWindow.render();
// });