// const controlPanel = './controlPanel.html'

const fullScreenRenderer = vtk.Rendering.Misc.vtkFullScreenRenderWindow.newInstance({background:[0,0,0,0]});
const vtkColorMaps = vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps;


const sliceurls = ['https://joshmillar.gitlab.io/vtks/static/simple_office/plane_x.vtp',
                    'https://joshmillar.gitlab.io/vtks/static/simple_office/plane_y.vtp',
                    'https://joshmillar.gitlab.io/vtks/static/simple_office/plane_z.vtp'   
                ];

const stlurls = [{url: 'https://joshmillar.gitlab.io/vtks/static/simple_office/walls.stl', opacity: "0.3"},
                {url: 'https://joshmillar.gitlab.io/vtks/static/simple_office/furniture.stl', opacity: "1.0"}
                ];

const streamurls = ['https://joshmillar.gitlab.io/vtks/static/simple_office/streamlines.vtp'];

var renderer = fullScreenRenderer.getRenderer();
var renderWindow = fullScreenRenderer.getRenderWindow();
renderer.getActiveCamera().setPosition(8, -9, 9.6);
renderer.getActiveCamera().setFocalPoint(0.2, 4.5, 0.4);
renderer.getActiveCamera().setViewUp(-0.25, 0.4, 0.9);


function displayStl(file) {
    stlurl = file.url;
    var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
        scalarVisibility: false,
    });
    var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
    var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

    stlActor.getProperty().setOpacity( file.opacity );
    stlActor.setMapper(stlMapper);
    stlMapper.setInputConnection(stlReader.getOutputPort());
    stlActor.getProperty().setColor(1, 1, 1);

    stlReader.setUrl(stlurl).then(() => {
        renderer.addActor(stlActor);
        renderer.resetCamera();
        renderWindow.render();
    });

}

for (var i = stlurls.length -1; i >= 0; i--)
{
    displayStl(stlurls[i]);
}

function displaySlice(url, remove) {
    // renderer.removeAllActors();
    var lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
    var preset = vtkColorMaps.getPresetByName('Rainbow Desaturated');
    lookupTable.applyColorMap(preset);

    var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: true,
        useLookupTableScalarRange: true,
        lookupTable,
        scalarVisibility: true,
    });
    var arrayName = 'MeanVelocityMagnitude';
    // vtkMapper.setScalarModeToUsePointData();
    vtkMapper.setScalarModeToDefault();
    vtkMapper.setColorModeToMapScalars();
    vtkMapper.setColorByArrayName(arrayName);
    vtkMapper.setScalarRange([0, 1]);

    var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
    var vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

    vtkMapper.setInputConnection(vtpReader.getOutputPort());
    vtkActor.setMapper(vtkMapper);

    function addActors() {
        renderer.addActor(vtkActor);
        renderWindow.render();
    }
    function removeActors() {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }

    if(remove==true){
        renderer.removeActor(vtkActor);
        renderWindow.render();
        console.log(renderer.getActors());
        // removeActors;
        // console.log(renderer.getActors());
        // vtpReader.setUrl(url).then(removeActor);
    } else {
        console.log("removefalse");
        vtpReader.setUrl(url).then(() => {
            renderer.addActor(vtkActor);
            renderWindow.render();
        });
        console.log(renderer.getActors());
    }
    console.log(renderer.getActors());
}

displaySlice(sliceurls[1], false)

fullScreenRenderer.addController('<table><tr><td>x-plane</td><td><input class="xplane" type="checkbox" name="xcheck"/></td></tr><tr><td>y-plane</td><td><input class="yplane" type="checkbox" checked name="ycheck"/></td></tr><tr><td>z-plane</td><td><input class="zplane" type="checkbox" name="zcheck"/></td></tr><tr><td>Streamlines</td><td><input class="streams" type="checkbox" name="streamcheck"/></td></tr></table>');

var xcheckbox = document.querySelector("input[name=xcheck]")
var ycheckbox = document.querySelector("input[name=ycheck]")
var zcheckbox = document.querySelector("input[name=zcheck]")
var streamcheckbox = document.querySelector("input[name=streamcheck]")

xcheckbox.addEventListener('change', function() {
 if(this.checked){
    displaySlice(sliceurls[0], false)
 } else {
    displaySlice(sliceurls[0], true)
 }
});
ycheckbox.addEventListener('change', function() {
 if(this.checked){
    displaySlice(sliceurls[1], false)
 } else {
    displaySlice(sliceurls[1], true)
 }
});
zcheckbox.addEventListener('change', function() {
 if(this.checked){
    displaySlice(sliceurls[2], false)
 } else {
    displaySlice(sliceurls[2], true)
 }
});
streamcheckbox.addEventListener('change', function() {
 if(this.checked){
    displaySlice(streamurls[0], false)
 } else {
    displaySlice(streamurls[0], true)
 }
});

// // const url = 'https://joshmillar.gitlab.io/vtks/static/simple_office/plane_x.vtp'
// const vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

// // // console.log(vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps.rgbPresetNames);

// const lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
// // // const preset = vtkColorMaps.getPresetByName('erdc_rainbow_bright');
// // // const preset = vtkColorMaps.getPresetByName('Rainbow Desaturated');
// const preset = vtkColorMaps.getPresetByName('jet');
// lookupTable.applyColorMap(preset);
// // // lookupTable.setMappingRange([0, 10])
// // // lookupTable.updateRange();

// const renderer = fullScreenRenderer.getRenderer();
// renderer.getActiveCamera().setPosition(8, -9, 9.6);
// renderer.getActiveCamera().setFocalPoint(0.2, 4.5, 0.4);
// renderer.getActiveCamera().setViewUp(-0.25, 0.4, 0.9)
// const renderWindow = fullScreenRenderer.getRenderWindow();
// // var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
// //         interpolateScalarsBeforeMapping: false,
// //         useLookupTableScalarRange: false,
// //         lookupTable,
// //         scalarVisibility: true,
// // });

// const arrayName = 'MeanVelocityMagnitude';

// vtkMapper.setScalarModeToUsePointData();
// vtkMapper.setScalarModeToDefault();
// vtkMapper.setColorModeToMapScalars();
// vtkMapper.setColorByArrayName(arrayName);
// vtkMapper.setScalarRange([0, 1])

// sliceurls.forEach((url) => {
//     console.log(url)
//     vtpReader.setUrl(url).then(() => {
//         var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
//         vtkMapper.setInputConnection(vtpReader.getOutputPort());
//         vtkActor.setMapper(vtkMapper);
//         renderer.addActor(vtkActor);
//         renderWindow.render();
//     });  
// });


// // // stls
// let stlurl = 'https://joshmillar.gitlab.io/vtks/static/BDGS.stl';
// var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
//     scalarVisibility: false,
// });
// var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
// var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

// stlActor.setMapper(stlMapper);
// stlMapper.setInputConnection(stlReader.getOutputPort());
// stlActor.getProperty().setColor(1, 1, 1);
// // stlActor.getProperty().setEdgeVisibility(false);
// // stlActor.getProperty().setEdgeColor(0, 0, 0);

// stlReader.setUrl(stlurl).then(() => {
//     renderer.addActor(stlActor);
//     renderer.resetCamera();
//     renderWindow.render();
// });

// // extract feature edges
// let edgeurl = 'https://joshmillar.gitlab.io/vtks/static/edges.vtp';

// var edgeMapper = vtk.Rendering.Core.vtkMapper.newInstance({
//     scalarVisibility: false,
// });
// var edgeActor = vtk.Rendering.Core.vtkActor.newInstance();
// var edgeReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

// edgeActor.setMapper(edgeMapper);
// edgeMapper.setInputConnection(edgeReader.getOutputPort());
// edgeActor.getProperty().setColor(0, 0, 0);

// edgeReader.setUrl(edgeurl).then(() => {
//     renderer.addActor(edgeActor);
//     renderer.resetCamera();
//     renderWindow.render();
// })

// fullScreenRenderer.addController('<table><tr><td>Streamlines</td><td><input class="streamlines" type="checkbox" name="streamcheck"/></td></tr></table>');

// var streamcheckbox = document.querySelector("input[name=streamcheck]")

// let streamurl = 'https://joshmillar.gitlab.io/vtks/static/streams.vtp';
// var streamMapper = vtk.Rendering.Core.vtkMapper.newInstance({
//     scalarVisibility: true,
// });
// streamMapper.setScalarModeToDefault();
// streamMapper.setColorModeToMapScalars();
// streamMapper.setScalarRange([0, 10])
// var streamActor = vtk.Rendering.Core.vtkActor.newInstance();
// var streamReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

// streamActor.setMapper(streamMapper);
// streamMapper.setInputConnection(streamReader.getOutputPort());

// streamcheckbox.addEventListener('change', function() {
// 	if(this.checked){
// 		streamReader.setUrl(streamurl).then(() => {
//             renderer.addActor(streamActor);
//             renderWindow.render();
//         });
// 	} else {
// 		renderer.removeActor(streamActor);
//         renderWindow.render();
// 	}
// });

// // document.querySelector('.edgeVisibility').addEventListener('change', (e) =>{
// //     const edgeVisibility = !!e.target.checked;
// //     stlActor.getProperty().setEdgeVisibility(edgeVisibility);
// //     console.log(stlActor.getProperty().setEdgeVisibility(edgeVisibility));
// //     renderer.resetCamera();
// //     renderWindow.render();
// // });